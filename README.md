# Systemd AdHoc  Services

https://ieeexplore.ieee.org/document/6477626
A dynamic network gateway selection scheme based on autonomous clustering for heterogeneous mobile ad hoc network environment


systemd service scripts to configure ad-hoc networks and NAT masquerading to provide quick forwarding support.

# Installation
To install
```bash
git clone https://gitlab.com/sweeneytr/systemd-adhoc-services.git
cd systemd-adhoc-services
./install.sh
```

To uninstall (Be sure to disable first!)
```bash
cd systemd-adhoc-services
./uninstall.sh
```

# Enabling / Disabling Interfaces
SSID is the name of the network. Typically we run over band 10 (2457), but you can run over any WLAN bands availible in the 2.4 and 5 GHz range. [WLAN Address wiki page](https://en.wikipedia.org/wiki/List_of_WLAN_channels).

To enable
```bash
network-wireless-adhoc config $DEV $ADDRESS/$MASK $SSID $FREQUENCY 
systemctl enable network-wireless-adhoc@$DEV
systemctl start network-wireless-adhoc@$DEV
```

To disable
```bash
systemctl stop network-wireless-adhoc@$DEV
systemctl disable network-wireless-adhoc@$DEV
```
