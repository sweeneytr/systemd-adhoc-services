#!/bin/bash

Q=""
USAGE="Usage: $0 {stop ifname | start ifname | config ifname ip/mask ssid freq | init | finalize}"

NM_CONF_D="/etc/NetworkManager/conf.d"
NM_CONF="$NM_CONF_D/network-wireless-adhoc.conf"

CONF_D="/etc/conf.d/"
CONF_D_IF="/etc/conf.d/network-wireless-adhoc"
CONF_D_NM_D="/etc/conf.d/NetworkManager_unmanage.d/"
CONF_D_NM="$CONF_D/network-wireless-adhoc.conf"

function main() 
{
	assert_systemd

	case "$1" in
		start)
			if [ $# != 2 ]; then usage; fi
			do_start $2 
			;;
		stop)
			if [ $# != 2 ]; then usage; fi
			do_stop $2
			;;
		config)
			if [ $# != 5 ]; then usage; fi
			do_conf ${@:2}
			;;
		init)
			if [ $# != 1 ]; then usage; fi
			do_init
			;;
		finalize)
			if [ $# != 1 ]; then usage; fi
			do_finalize
			;;
		*) 
			usage
			;;
	esac
}

function assert_systemd()
{
	NAME_PID1=$(ps --no-headers -o comm 1)
	if [ "$NAME_PID1" != "systemd" ]; then
		echo "ERROR: Not a System-D based distro, failing"
		exit 1
	fi
}


function check_nm()
{
	# Return code from systemctl is returned from this, tests if NM alive
	systemctl status NetworkManager > /dev/null
}



function usage()
{
	echo $USAGE
	exit 1
}


function do_start()
{
	if [[ ! $(ls /sys/class/net/$1) ]]; then
		echo "Device $1 not found"
		exit 1
	fi

	source $CONF_D_IF@$1
	nm_unmanage_device $1
	configure_device $1
        join_adhoc $1 $ssid $freq $addr $mask
}


function do_stop()
{
	if [[ ! $(ls /sys/class/net/$1) ]]; then
		echo "Device $1 not found"
		exit 1
	fi

	source $CONF_D_IF@$1
	leave_adhoc $1
	nm_manage_device $1
}


function do_conf()
{
	CONF=$(printf "addr=%s\nmask=%s\nssid=%s\nfreq=%s\n" \
		$(cut -d/ -f1 <<< $2) \
		$(cut -d/ -f2 <<< $2) \
		$3 \
		$4)

	CONF_D_NAME=$CONF_D_IF@$1
	# invoking bash here so when running w/ Q=echo doesn't try to redirect
	$Q bash -c "echo '$CONF' > $CONF_D_NAME"
}


function nm_manage_device()
{
	if [ check_nm ]
	then
		$Q nmcli device set ifname $1 managed yes
	fi
}


function nm_unmanage_device()
{
	MAC=$(cat /sys/class/net/$1/address)
	NMCONF=$(printf "mac:$MAC\n")
	NMCONF_D_NAME=$CONF_D_NM_D/network-wireless-adhoc@$1.conf

	if [ check_nm ]
	then	
		$Q nmcli device set ifname $1 managed no
	fi

	$Q bash -c "echo '$NMCONF' > $NMCONF_D_NAME"
}

function configure_device()
{
	$Q rfkill unblock wifi
	$Q ip link set $1 down
	$Q iw dev $1 set type ibss
	$Q ip link set $1 up	
}

function join_adhoc()
{
	$Q iw dev $1 ibss join $2 $3
	$Q ip addr flush dev $1
	$Q ip addr add $4/$5 dev $1
}

function leave_adhoc()
{
	$Q ip addr flush dev $1
	$Q ip link set $1 down
}

function do_init()
{
	mkdir -p $CONF_D_NM_D
	rm -f $CONF_D_NM_D/*.conf $NM_CONF
}

function do_finalize()
{
	FIRST=true
	CONF=$"[keyfile]\nunmanaged-devices="
	for f in $CONF_D_NM_D/*.conf
	do
		if [ "$FIRST" = true ]; then
			CONF=$"$CONF$(cat $f)"
			FIRST=false
		else
			CONF=$"$CONF;$(cat $f)"
		fi
	done
	CONF=$"$CONF\n"
	$Q bash -c "printf \"$CONF\" > $CONF_D_NM"
	$Q touch $NM_CONF
	mount -o ro,bind $CONF_D_NM $NM_CONF
}

main $@
