#!/bin/bash

cp network-wireless-adhoc /usr/local/bin/

for f in systemd/network-wireless-adhoc*.service
do
	cp $f /lib/systemd/system
done
